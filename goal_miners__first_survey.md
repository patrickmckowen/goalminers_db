



You think you know business? Take our quiz and find out how well attuned you are to the demands of being an entrepreneur.

Common characteristics in areas such as family background, childhood 
experiences, core values, personalities and more turn up time and time 
again in studies of entrepreneurs. Find out how you fit the mold by 
determining your Entrepreneurial Quotient, or EQ. The following test is 
no measure of your future success, but it may show you where you excel 
and where you need to improve to help make your business soar. Answer 
the following questions with a "yes" or "no," and total your score at 
the end to find out your EQ.

https://www.entrepreneur.com/article/247560


Starting out by looking up other tests as well as important underpinning 
elements to develop my own
