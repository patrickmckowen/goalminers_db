

def generate_items_list(number_of_items, start=0):
    string = ''
    if start != 0:
        response = 8
    for i in range(1, number_of_items):
        if response < 7:
            response = response + 1
        elif response > 1:
            response = response - 1
        string = string + 'item{0}={1}, '.format(i, response)
    print(string)

def generate_init_args(num_of_items):
	string = ''
    for i in range(1, num_of_items):
        string = string + 'item{0}, '.format(i)
    print(string)
