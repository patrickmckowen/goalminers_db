from .db import *
from .lookup_tables import *
from .goalminer import *
from .parent import *
from .entre_apt_survey import *

def prep_db(session):
    goalminer_1 = Goalminer(f_name = 'Steve', 
                            l_name = 'Stevenson', 
                            grade_level = 7, 
	                        phone = '1-304-555-5555', 
	                        email = 'steve@steve.com', 
	                        birthday = datetime(1999, 6, 5), 
	                        round_num = 1, 
	                        notes = 'Random notes about Steve')
	                        
    goalminer_2 = Goalminer(f_name = 'P', 
                            l_name = '-Money',
                            grade_level = 12, 
	                        phone = '3044104444', 
	                        email ='profpmoney@gmail.com',
	                        birthday = datetime(1990, 9, 19), 
	                        round_num = 1, 
	                        notes = 'P-Money rocks!')
	
    session.add(goalminer_1)
    session.add(goalminer_2)
    session.commit()
	                       
    parent_1 = Parent(f_name = 'p_f_name_1',
                      l_name = 'p_l_name_1',
                      phone = '304-235-5426',
	                  email = 'slug@monkey.com',
	                  birthday = datetime(1980, 7, 12),
	                  goalminers = [goalminer_1],
	                  notes = 'Random parents notes.')
	                            
    parent_2 = Parent(f_name = 'p_f_name_2', 
                      l_name = 'p_l_name_2', 
                      phone = '304-597-2049',
	                  email = 'porky@gerbel.com', 
	                  birthday = datetime(1970, 4, 18),
	                  goalminers = [goalminer_1],
	                  relationship = 'Father',
	                  notes = 'More random parents notes.')
	                            
    parent_3 = Parent(f_name = 'p_f_name_3',
                      l_name = 'p_l_name_4', 
                      phone = '304-305-2063',
	                  email = 'squirrlly@chipmonk.com', 
	                  birthday = datetime(1973, 4, 20),
	                  goalminers = [goalminer_2], 
	                  relationship = 'Mother',
	                  notes = 'Final random parents notes.')
	  
    session.add(parent_1)
    session.add(parent_2)
    session.add(parent_3)
    session.commit()
	                       
    survey1 = Entre_apt_survey(email = 'test_email_1@test.com', 
                               item1=1, item2=2, item3=3, item4=4, 
                               item5=5, item6=6, item7=7, item8=6, 
                               item9=7, item10=6, item11=7, item12=6, 
                               item13=7, item14=6, item15=7, item16=6, 
                               item17=7, item18=6, item19=7, item20=6, 
                               item21=7, item22=6, item23=7, item24=6, 
                               item25=7, item26=6, item27=7, item28=6, 
                               item29=7, item30=6, item31=7, item32=6, 
                               item33=7, item34=6, item35=7, item36=6, 
                               item37=7, item38=6, item39=7, item40=6, 
                               item41=7, item42=6, item43=7, item44=6, 
                               item45=7, item46=6, item47=7, item48=6, 
                               item49=7, item50=6)
                               
    survey2 = Entre_apt_survey(email = 'test_email_1@test.com',
                               item1=7, item2=6, item3=7, item4=6, 
                               item5=7, item6=6, item7=7, item8=6, 
                               item9=7, item10=6, item11=7, item12=6, 
                               item13=7, item14=6, item15=7, item16=6, 
                               item17=7, item18=6, item19=7, item20=6, 
                               item21=7, item22=6, item23=7, item24=6, 
                               item25=7, item26=6, item27=7, item28=6, 
                               item29=7, item30=6, item31=7, item32=6, 
                               item33=7, item34=6, item35=7, item36=6, 
                               item37=7, item38=6, item39=7, item40=6, 
                               item41=7, item42=6, item43=7, item44=6, 
                               item45=7, item46=6, item47=7, item48=6, 
                               item49=7, item50=6)
                               
    session.add(survey1)
    session.add(survey2)
    session.commit()
    

    goalminer_1.entre_apt_survey = survey1
    goalminer_2.entre_apt_survey = survey2
    
    session.add(goalminer_1)
    session.add(goalminer_2)
    session.commit()
