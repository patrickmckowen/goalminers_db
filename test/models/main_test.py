import unittest
from lib.models import *
from test.models.prep_db import *

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
log.addHandler(console_handler)
log.debug("Testing logger configured")

class TestMain(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' This is run once before any tests. '''
        dal.conn_string = 'sqlite:///:memory:'     #
        dal.connect()                              #
        dal.session = dal.Session()                #
        prep_db(dal.session)                       #
        dal.session.close()                        #
        
    def setUp(self):
        ''' This runs before each test. '''
        dal.session = dal.Session()                #

    def tearDown(self):
        ''' This happens after each test. '''      # DOCSTRING
        dal.session.rollback()                     # Undo db changes
        dal.session.close()                        # Close db connection

    # Add test functions here

    def test_check_if_goalminers_are_inserted(self):
        ''' Inserted three items. Since we start counting at 0, there should be 2. '''
        goalminers = dal.session.query(Goalminer).all()
        log.debug('All goalminers: ' + str(goalminers))
        self.assertEqual(len(goalminers), 2)
        
    def test_get_goalminers_attribute_of_parent(self):
        parent = dal.session.query(Parent).first()
        log.debug('Goalminers attribute of parent' + str(parent.goalminers))
        self.assertEqual(len(parent.goalminers), 1)

    def test_get_parent_attribute_of_goalminer(self):
        gm = dal.session.query(Goalminer).filter_by(f_name='Steve').first()
        log.debug('Parent attribute of goalminer: ' + str(gm.parents))
        self.assertEqual(len(gm.parents), 2)
    
    def test_get_survey_attribute_of_goalminer(self):
        gm = dal.session.query(Goalminer).filter_by(f_name='Steve').first()
        log.debug('Entre_apt_survey attribute of goalminer: ' + str(gm.entre_apt_survey.email))
        self.assertEqual(gm.entre_apt_survey.email, 'test_email_1@test.com')

    def test_get_application_attribute_of_goalminer(self):
        gm = dal.session.query(Goalminer).filter_by(f_name='Steve').first()
        log.debug('Application attribute of goalminer: ' + str(gm.applications))
        self.assertEqual(len(gm.applications), 1)
    
    def test_get_survey_attribute_of_goalminer(self):
        gm = dal.session.query(Goalminer).filter_by(f_name='Steve').first()
        log.debug('Email attribute of application on goalminer: ' + str(gm.applications[0].email))
        self.assertEqual(gm.applications[0].email, 'sloth@monkey.com')
        
    """
    def test_whatever(self):
        ''' Give two args that should be equal. '''
        self.assertEqual()

    def test_assert_true(self):
        ''' Single value that should return True. '''
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_assert_false(self):
        ''' Single value that should return False. '''
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        ''' Test for the correct rasing of an error. '''
        s = 'hello world'
        with self.assertRaises(TypeError):
            s.split(2)

    def test_insert_name(self):
        expected = {'name' : 'Steve'}
        #actual= ''
        http = httplib2.Http()
        body = {'name': 'Steve'}
        content = http.request("http://127.0.0.1:5000/",
                       method="POST",
                       headers={'Content-type': 'application/x-www-form-urlencoded'},
                       body=urllib.parse.urlencode(body) )[1]
        actual = json.loads(str(content))
        self.assertEqual( actual, expected)
    """

if __name__ == '__main__':
    unittest.main()
