'''




'''
from .db import *

goalminer_parent_table = Table('goalminer_parent', Base.metadata,
    Column('goalminer_id', Integer, ForeignKey('goalminer.id'), nullable=False),
    Column('parent_id', Integer, ForeignKey('parent.id'), nullable=False)
)

goalminer_application_table = Table('goalminer_application', Base.metadata,
    Column('goalminer_id', Integer, ForeignKey('goalminer.id'), nullable=False),
    Column('application_id', Integer, ForeignKey('application.id'), nullable=False)
)
