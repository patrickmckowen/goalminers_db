from .db import *

class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer(), primary_key=True)
    f_name = Column(String(25))
    l_name = Column(String(25))
    phone = Column(String(50))
    email = Column(String(50))
    birthday = Column(DateTime())
    #goalminers= relationship('Goalminer', secondary='goalminer_parent', backref='parents')
    # Mother, father etc
    relationship = Column(String(20))
    notes = Column(String(1000))
    created_on = Column(DateTime(), default=datetime.now)
    updated_on = Column(DateTime(), default=datetime.now, 
                        onupdate=datetime.now)

    def __init__(self, f_name, l_name, phone,
                 email, birthday, goalminers, 
                 relationship=None, notes=None):
        self.f_name = f_name
        self.l_name = l_name
        self. phone = phone
        self.email = email
        self. birthday = birthday
        #self.round_num = round_num
        self.goalminers = []
        self.goalminers = goalminers
        self.relationship = relationship
        self.notes = notes
        

    def __repr__(self):
        return "Parent: {} {}".format(self.f_name, self.l_name)
