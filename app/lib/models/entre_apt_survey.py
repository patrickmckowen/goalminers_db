from .db import *

#
class Entre_apt_survey(Base):
    __tablename__ = 'entre_apt_survey' # Lowercase and plural
    id = Column(Integer(), primary_key=True)
    # Email
    email = Column(String(50))
    item1 = Column(Integer())
    item2 = Column(Integer())
    item3 = Column(Integer())
    item4 = Column(Integer())
    item5 = Column(Integer())
    item6 = Column(Integer())
    item7 = Column(Integer())
    item8 = Column(Integer())
    item9 = Column(Integer())
    item10 = Column(Integer())
    item11 = Column(Integer())
    item12 = Column(Integer())
    item13 = Column(Integer())
    item14 = Column(Integer())
    item15 = Column(Integer())
    item16 = Column(Integer())
    item17 = Column(Integer())
    item18 = Column(Integer())
    item19 = Column(Integer())
    item20 = Column(Integer())
    item21 = Column(Integer())
    item22 = Column(Integer())
    item23 = Column(Integer())
    item24 = Column(Integer())
    item25 = Column(Integer())
    item26 = Column(Integer())
    item27 = Column(Integer())
    item28 = Column(Integer())
    item29 = Column(Integer())
    item30 = Column(Integer())
    item31 = Column(Integer())
    item32 = Column(Integer())
    item33 = Column(Integer())
    item34 = Column(Integer())
    item35 = Column(Integer())
    item36 = Column(Integer())
    item37 = Column(Integer())
    item38 = Column(Integer())
    item39 = Column(Integer())
    item40 = Column(Integer())
    item41 = Column(Integer())
    item42 = Column(Integer())
    item43 = Column(Integer())
    item44 = Column(Integer())
    item45 = Column(Integer())
    item46 = Column(Integer())
    item47 = Column(Integer())
    item48 = Column(Integer())
    item49 = Column(Integer())
    item50 = Column(Integer())
    goalminer_id = Column(Integer, ForeignKey('goalminer.id'), nullable=True)
    created_on = Column(DateTime(), default=datetime.now)
    updated_on = Column(DateTime(), default=datetime.now, 
                        onupdate=datetime.now)


    def __init__(self, email, item1, item2, item3, item4, item5, item6, 
                             item7, item8, item9, item10, item11, item12, 
                             item13, item14, item15, item16, item17, 
                             item18, item19, item20, item21, item22, 
                             item23, item24, item25, item26, item27, 
                             item28, item29, item30, item31, item32, 
                             item33, item34, item35, item36, item37, 
                             item38, item39, item40, item41, item42, 
                             item43, item44, item45, item46, item47, 
                             item48, item49, item50):
        self.__dict__.update({k: v for k, v in locals().items() if k != 'self'})
