'''



'''
import pandas as pd
from pandas import DataFrame


class Goalminer(object):
    
    df = DataFrame()
    
    @staticmethod
    def write_goalminer_personal_info_csv(series):
        path = os.path.join(series['first_name'].lower().split()[0], 'personal_info.csv')
        with open(path, 'w+') as csvfile:
            fieldnames = series.index.tolist()
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerow(series.to_dict())
        
    @staticmethod
    def update_from_dataframe():
        for index, row in Goalminer.df.iterrows():
            write_goalminer_personal_info_csv(row)
            
    @staticmethod
    def get_goalminer_dirs():
        return [ i for i in os.listdir() if 
            os.path.isdir(i) and 
            i != '__init__.py']
    
    def __init__(self, f_nm, m_nm, l_nm, eamil, em_con, schl, hobb,
                 par_orgn, drm_jb, app_start, app_submit):
        self.f_name = f_nm
        self.m_name = m_nm
        self.l_name = l_nm
        self.email = email
        self.emergency_contact = em_con
        self.school = schl
        self.hobbies = hobb
        self.parent_origin = par_orgn
        self.dream_job = drm_jb
        self.application_start_date = app_start
        self.application_submit_date = app_submit
        
