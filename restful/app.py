from flask import Flask, jsonify
from flask_restful import Resource, Api
from lib.models import *
from lib.models.prep_db import prep_db


import logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
log.addHandler(console_handler)
log.debug("logger configured")

app = Flask(__name__)

api = Api(app)

def set_up_db(dal):
	log.debug('Beggining of "setu_up_db" function')
	dal.connect()               #
	dal.session = dal.Session() #
    #prep_db(dal.session)        #

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}
		
class Goalminers(Resource):
    def get(self):
        response_obj = dal.session.query(Goalminer).all()
        log.debug(str(dir(response_obj[0])))
        return jsonify(dal.session.query(Goalminer).all())
        
class Goalminer_resource(Resource):
    def get(self, goalminer_id):
        response_obj = dal.session.query(Goalminer).filter_by(id=goalminer_id).first()
        return response_obj.__dict__
        
######################################################
## Actually setup the Api resource routing here
##
api.add_resource(HelloWorld, '/')
#log.info('')
#log.debug('')


api.add_resource(Goalminers, '/goalminers')
log.debug('Called api.add_resource. Passed "Goalminer_resource" class and specified "/goalminers" path.')
log.info('"/goalminers" resource added.')

api.add_resource(Goalminer_resource, '/goalminers/<goalminer_id>')
log.info('Resource added for specifying a specific goalminer')
log.debug('Called api.add_resource. Passed "Goalminer_resource" class and specified "/goalminers/<goalminer_id>" path.')

if __name__ == '__main__':
    set_up_db(dal)
    app.run(debug=True)
    
