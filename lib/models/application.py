'''
What are 3 of your favorite hobbies?
Where did your parents (or guardians) grow up?
What is your dream job?	<em>The essay portion must be typed.
<br>
It must be emailed to candice@createwv.org.<br>500 words minimum - 1000 
words maximum. Double spaced.<br>Please read the prompt carefully. 
<br><br>Your dear Grandpa Mason has passed away. In his will, he left 
you with most of his estate which includes an inheritance of $50,000 and
his beloved 50 acre farm in Paw Paw Creek, West Virginia. There is a 
letter addressed to you in the will:<br><br></em>
Start Date (UTC)
Submit Date (UTC)
Network ID

'''
from .db import *

class Application(Base):
    __tablename__ = 'application'
    id = Column(Integer(), primary_key=True)
    full_name = Column(String(25))
    email = Column(String(50))
    emergency_contact = Column(String(65))
    school = Column(String(25))
    hobbies = Column(String(50))
    dream_job = Column(String(25))
    essay = Column(String(5000))
    started_on = Column(DateTime())
    submited_on = Column(DateTime())
    notes = Column(String(1000))
    created_on = Column(DateTime(), default=datetime.now)
    updated_on = Column(DateTime(), default=datetime.now, 
                        onupdate=datetime.now)

    def __repr__(self):
        return "Registration: {}".format(self.full_name)
