from .imports import *

Base = declarative_base()


class DataAccessLayer(object):
	
    def __init__(self, conn_string='sqlite:///test.db'):
        log.debug("Begin initializing DataAcessLayer instance.")
		
        self.engine = None
        log.debug("self.engine initialized with None")
        
        self.conn_string = conn_string
        log.debug("self.engine initialized with {}".format(self.conn_string))
			
        log.info("Data Access Layer Initialized")
        log.debug("Finished initializing DataAcessLayer instance")

    def connect(self):
        self.engine = create_engine(self.conn_string)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
        log.info("Database connection created: {}".format(self.conn_string))

dal = DataAccessLayer()
