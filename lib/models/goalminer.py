from .db import *

class Goalminer(Base):
	#
    __tablename__ = 'goalminer'
    #
    id = Column(Integer(), primary_key=True)
    # First Name
    f_name = Column(String(25))
    # Last Name
    l_name = Column(String(25))
    # Grade Level
    grade_level = Column(Integer())
    # Phone
    phone = Column(String(50))
    # Email
    email = Column(String(50))
    # Birthday
    birthday = Column(DateTime())
    # Round # for the workshop series
    round_num = Column(Integer())
    # RELATIONSHIP
    # no foreign key here, it's in the many-to-many table
    # mapped relationship, goalminer_parent_table must already be in scope!
    parents = relationship('Parent', secondary='goalminer_parent', backref='goalminers')
    # no foreign key here, it's in the many-to-many table
    # mapped relationship, goalminer_parent_table must already be in scope!
    applications = relationship('Application', secondary='goalminer_application', backref='goalminers')
    #
    entre_apt_survey = relationship("Entre_apt_survey", uselist=False,
                                     backref='goalminers')
    # Notes
    notes = Column(String(1000))
    # Created
    created_on = Column(DateTime(), default=datetime.now)
    # Updated
    updated_on = Column(DateTime(), default=datetime.now,
                        onupdate=datetime.now)
    '''
    def __init__(self, f_name, l_name, grade_level, 
                 phone, email, birthday, round_num, 
                 parents=[], entre_apt_survey=None, notes=None):
        self.f_name = f_name
        self.l_name = l_name
        self.grade_level = grade_level
        self.phone = phone
        self.email = email
        self. birthday = birthday
        self.round_num = round_num
        self.parents = parents
        self.entre_apt_survey = entre_apt_survey
        self.notes = notes
    '''
    def __repr__(self):
        return "Goalminer: {} {}".format(self.f_name, self.l_name)
