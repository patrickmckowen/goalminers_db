from flask_wtf import FlaskForm
from wtforms import TextField, IntegerField, TextAreaField, SubmitField, RadioField, SelectField, validators

likert = [('1','Strongly Disagree'),('2','Moderately Disagree'), ('3', 'Neither Agree Nor Disagree'), ('4', 'Moderately Agree'), ('5', 'Strongly Agree')]

class EntreSurveyForm(FlaskForm):
   #name = TextField("Name Of Student",[validators.Required("Please enter your name.")])
   item1 = RadioField('My parents taught me about starting a business', choices = likert)
   item2 = RadioField('My parents taught me about starting a business', choices = likert)
   item3 = RadioField('I have had conversations with my child about starting a business', choices = likert)
   item4 = RadioField('I have had conversations with my child about starting a business', choices = likert)
   item5 = RadioField('I encourage my child/children to consider starting businesses', choices = likert)
   item6 = RadioField('I know a lot of people who are business owners', choices = likert)
   item7 = RadioField('I am not familiar with the process of starting a business', choices = likert)
   item8 = RadioField('I have tried to be an entrepreneur ', choices = likert)
   item9 = RadioField('Starting a business is easy', choices = likert)
   submit = SubmitField("Send")
