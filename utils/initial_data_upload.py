import csv


csv_file_path = '/home/pi/Downloads/Goal Miners Registration.csv'

applications = []
num = 0
with open(csv_file_path) as csvfile:
    spamreader = csv.reader(csvfile)
    
    for row in spamreader:
        if num != 0:
            application = []
            application.append(row[2])
            application.append(row[5])
            application.append(row[8])
            application.append(row[11])
            application.append(row[14])
            application.append(row[17])
            application.append(row[20])
            application.append(row[23])
            application.append(row[26])
            applications.append(application)
        num += 1

print(applications)
