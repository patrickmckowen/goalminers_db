#
# There are three parts 
#   - You need to use a counter 
#   - You have a check condition
#   - Something that happens after the loop
#
#
#
#  - init -
#  - pre
#  - post
# ---------------------------------------------------------------------------- #
read -r -d '' forms <<'EOF'
from flask_wtf import Form
from wtforms import TextField

class ContactForm(Form):
   name = TextField("Name Of Student")
EOF

#echo "$forms" > forms.py

#source ./for_loop.go

# ---------------------------------------------------------------------------- #
read -r -d '' app <<'EOF'
from flask import Flask, render_template, request, jsonify
#from forms import ContactForm

app = Flask(__name__)
app.secret_key = 'development key'

@app.route('/', methods=['POST'])
def index():
   #form = ContactForm()
   #return render_template('contact.html', form = form)
   name = request.get_json
   return jsonify(name)

if __name__ == '__main__':
   app.run(debug = True)
EOF

echo "$app" > app.py

#source ./for_loop_w_o_initgo

# ---------------------------------------------------------------------------- #
read -r -d '' test <<'EOF'
import httplib2
import urllib
import unittest
import json

base_url = 'http://127.0.0.1/'

class TestMain(unittest.TestCase):
    
    def setUp(self):
        pass
 
    def test_insert_name(self):
        expected = {'name' : 'Steve'}
        #actual= ''
        http = httplib2.Http()
        body = {'name': 'Steve'}
        content = http.request("http://127.0.0.1:5000/",
                       method="POST",
                       headers={'Content-type': 'application/x-www-form-urlencoded'},
                       body=urllib.parse.urlencode(body) )[1]
        actual = json.loads(content)
        self.assertEqual( actual, expected)
 
if __name__ == '__main__':
    unittest.main()
EOF

echo "$test" > main_test.py

python3 app.py
