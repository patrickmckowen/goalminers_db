class Survey(object):
    ''' '''
    
    def to_dict(self):
        pass
    
    def to_series(self):
        series = Series(self.to_dict())
        series.index.name = 'questions'
        series.name = 'responses'
        return series

    def to_csv(self):
        return self.to_series().to_csv()

    def to_json(self):
        return self.to_series().to_json()

    def to_list(self):
        return self.to_series().tolist()