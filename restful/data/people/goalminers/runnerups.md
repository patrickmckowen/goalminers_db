  GNU nano 2.7.4                                                                                                                                                                                                                                           File: goalminers/runnerups.md                                                                                                                                                                                                                                           Modified  

If you have resource ideas please comment them (cntrl alt m) on the connecting words.
*Do we want to send resources to the kids who never sent their essays?*

* Nevaeh
  - Hobbies
   - Twirling
   - Modeling
   - Reading
  - Career Plan
   - She wants to be an entrepreneur.

* Nicholas
  - Hobbies
   - Hunting
   - Fishing
   - Making things
  - Dream Job
   - Mechanical engineer and/or a welder

* Darrell
  - Hobbies
   - Works with family business
   - Art
   - Playing outside in the dirt and mud
  - Dream Job
   - Wants to be a business owner

* Breeanna
  - Notes
   - No essay
  - Hobbies
   - Art
   - Slime
   - 4 Wheeling
  - Dream Job
   - Wants to be a CEO

* John Kincaid
  - Notes
   - No essay
  - Hobbies
   - Sports
   - Math
   - Collecting Jerseys
  - Dream Job
   - Wants to be a Stock Broker in NYC

* Tailey Clendenin
  - Notes
   - No essay
  - Hobbies
   - Lacrosse
   - Singing
   - Sleeping
  - Dream Job
   - Wants to be a neurosurgeon

* Jordan Hamon
  - Hobbies
   - Softball
   - Reading
   -Drawing
  - Dream Job
   - Wants to be a child psychologist

* Erin Skaff
  - Notes
   - Turned in application late
  - Hobbies
   - Riding horses
   - Snow skiing
   - Violin
  - Dream Job
   - Wants to be an international/immigrant attorney

* Neha
  - Notes
   - Parents are from India
  - Hobbies
   - Chess
   - Drawing/painting
   - Dance
  - Dream Job
   - Wants to be a doctor

* Jasmine
  - Hobbies
   - Reading
   - Drawing
   - Art
  - Dream Job
   - Wants to be a lawyer or a photographer

