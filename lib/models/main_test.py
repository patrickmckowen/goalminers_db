import unittest
from models import *
from prep_db import *

class TestMain(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' This is run once before any tests. '''
        dal.conn_string = 'sqlite:///:memory:'     #
        dal.connect()                              #
        dal.session = dal.Session()                #
        prep_db(dal.session)                       #
        dal.session.close()                        #
    def setUp(self):
        ''' This runs before each test. '''
        dal.session = dal.Session()                #

    def tearDown(self):
        ''' This happens after each test. '''      # DOCSTRING
        dal.session.rollback()                     # Undo db changes
        dal.session.close()                        # Close db connection

    # Add test functions here

    def test_get_all_goalminers():
        name = session.query(Goalminer).filter(Goalminer.f_name == 'Steve').all().f_name
        self.assertEqual(name, 'Steve')

    """
    def test_whatever(self):
        ''' Give two args that should be equal. '''
        self.assertEqual()

    def test_assert_true(self):
        ''' Single value that should return True. '''
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_assert_false(self):
        ''' Single value that should return False. '''
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        ''' Test for the correct rasing of an error. '''
        s = 'hello world'
        with self.assertRaises(TypeError):
            s.split(2)

    def test_insert_name(self):
        expected = {'name' : 'Steve'}
        #actual= ''
        http = httplib2.Http()
        body = {'name': 'Steve'}
        content = http.request("http://127.0.0.1:5000/",
                       method="POST",
                       headers={'Content-type': 'application/x-www-form-urlencoded'},
                       body=urllib.parse.urlencode(body) )[1]
        actual = json.loads(str(content))
        self.assertEqual( actual, expected)
    """

if __name__ == '__main__':
    unittest.main()
