# Goal Miners 2018
## Orientation Day Agenda 

* 10 am - Arrival and Introduction to the Project/Expectations (30 min)
* Paperwork and Sign the Contracts (20 minutes)
* 11 am - Short Break and Bye Bye Parents (15 minutes)
* Activity - (35 - 45 minutes)
* 12 PM Break for lunch (30 minutes)
* Activity - Strengths & Hobbies Shark Tank (1 hour)
* Short Break at 2 PM (10 minutes)
* Tour of the EDC & More (45 minutes)
* Team Rules and Expectations (30 minutes)
* 2:45 - Final Discussion & Communication (Parents Included)
* 3 PM - Head Home

