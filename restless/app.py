from flask import Flask, jsonify
from flask_restless import APIManager
from lib.models import *
from lib.models.prep_db import prep_db


import logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
log.addHandler(console_handler)
log.debug("logger configured")

app = Flask(__name__)

#def set_up_db(dal):
log.debug('Beggining of "setu_up_db" function')
dal.connect()               #
dal.session = dal.Session() #
    #prep_db(dal.session)        #

######################################################
## Actually setup the Api resource routing here
##

manager = APIManager(app, session=dal.session)
log.info('Manager created')


# Question: Do you use multiple api endpoints for a single model?
#

#
goalminer_blueprint = manager.create_api(Goalminer, methods=['GET'])
log.debug('"goalminer_blueprint" created with get method only.')

parent_blueprint = manager.create_api(Parent, methods=['GET'])
log.debug('"parent_blueprint" created with get method only.')

parent_blueprint = manager.create_api(Entre_apt_survey, methods=['GET'])
log.debug('"entre_apt_survey" created with get method only.')

if __name__ == '__main__':
    #set_up_db(dal)
    app.run(debug=True)

