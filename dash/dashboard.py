# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from tinydb import TinyDB, Query
import pandas as pd
from pandas import DataFrame, Series

app = dash.Dash()

def generate_table(dataframe, max_rows=10):
    '''Given dataframe, return template generated using Dash components
    '''
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

#df = pd.read_csv('data/people/goalminers/goalminers.csv')
#app.layout = html.Div(generate_table(df))




if __name__ == '__main__':
    app.run_server(debug=True)
