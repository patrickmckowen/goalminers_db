from .survey import Survey

class Survey(Survey):
    ''' Entrepreneurial Aptitude psychometric test. '''

    def __init__(self, email=None, item1=None, item2=None,
                 item3=None, item4=None, item5=None,
                 item6=None, item7=None, item8=None,
                 item9=None, item10=None, item11=None,
                 item12=None, item13=None, item14=None,
                 item15=None, item16=None, item17=None,
                 item18=None, item19=None, item20=None,
                 item21=None, item22=None, item23=None,
                 item24=None, item25=None, item26=None,
                 item27=None, item28=None, item29=None,
                 item30=None, item31=None, item32=None,
                 item33=None, item34=None, item35=None,
                 item36=None, item37=None, item38=None,
                 item39=None, item40=None, item41=None,
                 item42=None, item43=None, item44=None,
                 item45=None, item46=None, item47=None,
                 item48=None, item49=None, item50=None):
        self.__dict__.update({k: v for k, v in locals().items() if k != 'self'})

    def to_dict(self):
        return {'email':self.email,
                'Go straight for the goal':self.item1,
                'Have extra time on my hands':self.item2,
                'Listen to my feelings when making important decisions':self.item3,
                'Believe that unfortunate events occur because of bad luck':self.item4,
                'Like to solve complex problems':self.item5,
                'Do just enough work to get by':self.item6,
                "Don't quit a task before it is finished":self.item7,
                "Plan my life logically":self.item8,
                "Just know that I will be a success":self.item9,
                "Don't pride myself on being original":self.item10,
                "Turn plans into actions":self.item11,
                "Feel that work is not an important part of my life":self.item12,
                "Base my goals in life on inspiration, rather than logic":self.item13,
                "See difficulties everywhere":self.item14,
                "Ask questions that nobody else does":self.item15,
                "Have a slow pace to my life":self.item16,
                "Finish things despite obstacles in the way":self.item17,
                "Believe important decisions should be based on logical reasoning":self.item18,
                "Like to take responsibility for making decisions":self.item19,
                "Am not considered to have new and different ideas":self.item20,
                "Plunge into tasks with all my heart":self.item21,
                "Don't finish what I start":self.item22,
                "Plan my life based on how I feel":self.item23,
                "Feel that I'm unable to deal with things":self.item24,
                "Love to think up new ways of doing things":self.item25,
                "Am not highly motivated to succeed":self.item26,
                "Don't get sidetracked when I work":self.item27,
                "Listen to my brain rather than my heart":self.item28,
                "Believe that my success depends on ability rather than luck":self.item29,
                "Will not probe deeply into a subject":self.item30,
                "Try to surpass others' accomplishments":self.item31,
                "Give up easily":self.item32,
                "Believe emotions give direction to life":self.item33,
                "Dislike taking responsibility for making decisions":self.item34,
                "Am able to come up with new and different ideas":self.item35,
                "Put little time and effort into my work":self.item36,
                "Am a hard worker":self.item37,
                "Make decisions based on facts, not feelings":self.item38,
                "Feel up to any task":self.item39,
                "Am not interested in theoretical discussions":self.item40,
                "Continue until everything is perfect":self.item41,
                "Do not tend to stick with what I decide to do":self.item42,
                "Listen to my heart rather than my brain":self.item43,
                "Habitually blow my chances":self.item44,
                "Have an imagination that stretches beyond that of my friends":self.item45,
                "Find it difficult to get down to work":self.item46,
                "Put work above pleasure":self.item47,
                "Believe emotional factors to be of little importance":self.item48,
                "Make a decision and move on":self.item49,
                "Have difficulty understanding abstract ideas":self.item50
               }
    
    def load_from_csv(self):
        with open(os.path.join(os.path.dirname(__file__), 'init_entre_apt.csv')) as csvfile:
            csv_data = csv.reader(csvfile)
            for row in csv_data:
                print(', '.join(row))
