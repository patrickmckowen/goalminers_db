from .survey import Survey

class Survey(Survey):
    ''' Entrepreneurial Aptitude psychometric test. '''

    def __init__(self, child_email=None, relation=None,
                 item1=None, item2=None, item3=None,
                 item4=None, item5=None, item6=None,
                 item7=None, item8=None, item9=None):
        self.child_email = child_email
        self.relation = relation
        self.item1 = item1
        self.item2 = item2
        self.item3 = item3
        self.item4 = item4
        self.item5 = item5
        self.item6 = item6
        self.item7 = item7
        self.item8 = item8
        self.item9 = item9

    def to_dict(self):
        return {
                "Child's email":self.child_email,
                "relation":self.relation,
                'My parents taught me about starting a business':self.item1,
                'I have had conversations with my child about starting a business':self.item2,
                "I don't think it's a good idea to start a business in the current economy":self.item3,
                'I encourage my child/children to consider starting businesses':self.item4,
                'I know a lot of people who are business owners':self.item5,
                'I am not familiar with the process of starting a business':self.item6,
                "I have tried to be an entrepreneur ":self.item7,
                "Starting a business is easy":self.item8
               }
