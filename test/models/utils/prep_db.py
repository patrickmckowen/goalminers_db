def prep_db(session):
    goalminer_1 = Goalminer('Steve', 
                            'Stevenson', 
                            7, 
	                        '1-304-555-5555', 
	                        'steve@steve.com', 
	                        datetime(1999, 6, 5), 
	                        1, 
	                        'Random notes about Steve')
	                        
    goalminer_2 = Goalminer('P', 
                            '-Money',
                            12, 
	                        '3044104444', 
	                        'profpmoney@gmail.com',
	                        datetime(1990, 9, 19), 
	                        1, 
	                        'P-Money rocks!')
	
    session.bulk_save([goalminer_1, goalminer_2])
    session.commit()
	                        
    parent_1 = Goalminer_parent('p_f_name_1',
                                'p_l_name_1',
                                '304-235-5426',
	                            'slug@monkey.com',
	                            datetime(1980, 7, 12),
	                            'Random parents notes.',
	                            goal_miner_1, 
	                            'Mother')
	                            
    parent_2 = Goalminer_parent('p_f_name_2', 
                                'p_l_name_2', 
                                '304-597-2049',
	                            'porky@gerbel.com', 
	                            datetime(1970, 4, 18),
	                            'More random parents notes.',
	                            goalminer_1,
	                            'Father')
	                            
    parent_3 = Goalminer_parent('p_f_name_3',
                                'p_l_name_4', 
                                '304-305-2063',
	                            'squirrlly@chipmonk.com', 
	                            datetime(1973, 4, 20),
	                            'Final random parents notes.',
	                            goal_miner_2, 
	                            'Mother')
	  
    session.bulk_save([parent_1, parent_2, parent_3])
    session.commit()
	                       
    survey1 = Entre_apt_survey(email = 'test_email_1@test.com', 
                               item1=1, item2=2, item3=3, item4=4, 
                               item5=5, item6=6, item7=7, item8=6, 
                               item9=7, item10=6, item11=7, item12=6, 
                               item13=7, item14=6, item15=7, item16=6, 
                               item17=7, item18=6, item19=7, item20=6, 
                               item21=7, item22=6, item23=7, item24=6, 
                               item25=7, item26=6, item27=7, item28=6, 
                               item29=7, item30=6, item31=7, item32=6, 
                               item33=7, item34=6, item35=7, item36=6, 
                               item37=7, item38=6, item39=7, item40=6, 
                               item41=7, item42=6, item43=7, item44=6, 
                               item45=7, item46=6, item47=7, item48=6, 
                               item49=7, item50=6, goalminer=goalminer_1)
                               
    survey2 = Entre_apt_survey(email = 'test_email_1@test.com',
                               item1=7, item2=6, item3=7, item4=6, 
                               item5=7, item6=6, item7=7, item8=6, 
                               item9=7, item10=6, item11=7, item12=6, 
                               item13=7, item14=6, item15=7, item16=6, 
                               item17=7, item18=6, item19=7, item20=6, 
                               item21=7, item22=6, item23=7, item24=6, 
                               item25=7, item26=6, item27=7, item28=6, 
                               item29=7, item30=6, item31=7, item32=6, 
                               item33=7, item34=6, item35=7, item36=6, 
                               item37=7, item38=6, item39=7, item40=6, 
                               item41=7, item42=6, item43=7, item44=6, 
                               item45=7, item46=6, item47=7, item48=6, 
                               item49=7, item50=6, goalminer=goalminer_2)
                               
    session.bulk_save([survey_1, survey_2])
    session.commit()
