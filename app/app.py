from flask import Flask, render_template, request, jsonify
#from forms import *
import flask_admin
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from lib.models import *

#dal.conn_string = 'sqlite:///:memory:'     #
dal.connect()                              #
dal.session = dal.Session()                #  
        
admin = Admin(name='Goalminers')

app = Flask(__name__)
app.secret_key = 'development key'

#prep_db(dal.session)

admin.add_view(ModelView(Goalminer, dal.session))
admin.add_view(ModelView(Parent, dal.session))
#admin.add_view(ModelView(goalminer_parent_table, dal.session))
admin.add_view(ModelView(Entre_apt_survey, dal.session))
admin.init_app(app)  
@app.route('/')
def index():
   form = EntreSurveyForm()
   return render_template('index.html', form = form)

@app.route('/show_responses')
def show_response():
    responses = request.form()
    return jsonify(responses)


if __name__ == '__main__':
   app.run(debug = True)
